package autotest.pages.elements;


import autotest.core.annotations.DescriptionOfElement;
import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPage extends CommonElements{
    @DescriptionOfElement("Email or phone")
    @Getter
    @FindBy(id = "identifierId")
    private WebElement emailInput;

    @DescriptionOfElement("Password")
    @Getter
    @FindBy(xpath = "//h1[@id=\"headingText\"]/span")
    private WebElement SignInText;

    @DescriptionOfElement("кнопка входа - Next")
    @Getter
    @FindBy(xpath = "//div[@id='identifierNext']")
    private WebElement nextBtn;

    @DescriptionOfElement("Couldn’t find your Google Account")
    @Getter
    @FindBy(xpath = "(//div[@aria-live='assertive']/div/text())[1]")
    private WebElement negativeInfoText;


    @DescriptionOfElement("Couldn’t find your Google Account")
    @Getter
    @FindBy(xpath = "//h1[@id='headingText']/span")
    private WebElement cannotSignInText;


}
