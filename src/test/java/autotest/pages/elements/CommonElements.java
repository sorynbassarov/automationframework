package autotest.pages.elements;

import autotest.core.annotations.DescriptionOfElement;
import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static autotest.core.base.BaseTest.instantiateDriver;

/*
* Класс CommonElements хранит общие вебэлементы для всех страниц
* */
public class CommonElements {

    public CommonElements() {
        PageFactory.initElements(instantiateDriver(), this);
    }

    @Getter
    @DescriptionOfElement("логотип - Google")
    @FindBy(xpath = "//img[@alt='Google']")
    private WebElement logo;

    @Getter
    @DescriptionOfElement("кнопка - Sign in")
    @FindBy(xpath = "//*[@target=\"_top\" and text()=\"Sign in\"]")
    private WebElement signInBtn;

    @Getter
    @DescriptionOfElement("кнопка - Google Apps")
    @FindBy(xpath = "//*[@id=\"gbwa\"]/div/a/svg")
    private WebElement googleAppsMenuBtn;

    @Getter
    @DescriptionOfElement("Меню: поле - Images")
    @FindBy(xpath = "//a[@data-pid=\"2\"]")
    private WebElement imagesFld;

    @Getter
    @DescriptionOfElement("Меню: поле - Gmail")
    @FindBy(xpath = "//a[@data-pid=\"2\"]")
    private WebElement gmailFld;

}
