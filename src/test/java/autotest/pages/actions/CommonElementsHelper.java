package autotest.pages.actions;


import autotest.core.base.BasePage;
import autotest.core.base.Page;
import autotest.pages.elements.CommonElements;

public class CommonElementsHelper extends BasePage {
    private final CommonElements commonElements = new CommonElements();
    public CommonElementsHelper() {
        super();
    }

    public SignInPageHelper clickSignIn(){
        waitForElementVisibility(commonElements.getSignInBtn());
        commonElements.getSignInBtn().click();
        Page.logger.info("Sign In button was clicked");
        return new SignInPageHelper();
    }
}
