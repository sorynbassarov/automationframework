package autotest.pages.actions;

import autotest.core.base.BasePage;
import autotest.pages.elements.MainPage;

public class MainPageHelper extends BasePage {
    private final MainPage mainPage = new MainPage();

    public MainPageHelper() {
        super();
    }

    public void searchInGoogle(String searchingText){
        waitForElementVisibility(mainPage.getSearchInput());
        mainPage.getSearchInput().sendKeys(searchingText);
        waitForElementToBeClickable(mainPage.getGoogleSearchBtn());
        mainPage.getGoogleSearchBtn().click();
    }
}
