package autotest.tests.regress;


import autotest.core.base.BaseTest;
import autotest.core.data.DataProviderClass;
import org.testng.annotations.Test;


public class SearchTest extends BaseTest {
    SearchTest(){
        super();
    }

    @Test(dataProvider = "searchData", dataProviderClass = DataProviderClass.class)
    public void searchTest(String search){
        mainPageHelper.searchInGoogle(search);
    }
}
