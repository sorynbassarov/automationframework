package autotest.tests.regress;


import autotest.core.base.BaseTest;

import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest {
    LoginTest(){
        super();
    }

    @Test
    public void login(){
        String emailNotification = commonElementsHelper
                .clickSignIn()
                .checkForInvalidInput(getAddUserName());


        Assert.assertEquals(emailNotification, "Couldn’t sign you in");
    }

}